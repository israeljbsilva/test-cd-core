import os

from django.http import JsonResponse
from django.utils import timezone
from django.contrib.auth.models import User

from rest_framework import viewsets, mixins
from rest_framework.parsers import FileUploadParser
from rest_framework.response import Response
from rest_framework.exceptions import ValidationError

from drf_yasg.openapi import Parameter
from drf_yasg.utils import swagger_auto_schema

from config.settings.base import BASE_DIR
from test_cd_core.models import FilesUpload
from test_cd_core.serializers import FilesUploadSerializer


def ping(request):
    return JsonResponse({
        'request': f'{request.environ.get("REQUEST_METHOD")} '
        f'{request.environ.get("HTTP_HOST")}{request.environ.get("PATH_INFO")}',
        'timestamp': timezone.localtime(),
        'build_date': os.environ.get('BUILD_DATE'),
        'revision': os.environ.get('REVISION')})


class FileUploadViewSet(viewsets.GenericViewSet, mixins.ListModelMixin):
    queryset = FilesUpload.objects.all()
    serializer_class = FilesUploadSerializer

    @swagger_auto_schema(
        manual_parameters=[
            Parameter('user', 'query', description='The user id', required=True, type='string')
        ]
    )
    def list(self, request, *args, **kwargs):
        return super().list(request, *args, **kwargs)

    def get_queryset(self):
        user = self.request.query_params.get('user')
        queryset = FilesUpload.objects.filter(user=user)
        return queryset


class FileUpload(viewsets.ViewSet):
    parser_classes = (FileUploadParser, )

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    @swagger_auto_schema(
        manual_parameters=[
            Parameter('user', 'header', description='The user id', required=True, type='string'),
            Parameter('file', 'request_body', description='The file to upload.', required=True, type='file')
        ]
    )
    def create(self, request):
        user_id = request.environ.get('HTTP_USER')
        user = User.objects.get(pk=user_id)
        file = self._obtain_file(request)
        self._write_file(file, file.name)
        FilesUpload(file_name=file.name, user=user).save(force_insert=True)
        return Response()

    @staticmethod
    def _obtain_file(request):
        try:
            return request.FILES['file']
        except Exception:
            raise ValidationError('File not found.')

    @staticmethod
    def _write_file(file, file_name):
        with open(os.path.join(BASE_DIR, "uploads", file_name), 'wb') as saida:
            saida.write(file.read())

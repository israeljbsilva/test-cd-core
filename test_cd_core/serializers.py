from rest_framework import serializers
from .models import FilesUpload


class FilesUploadSerializer(serializers.ModelSerializer):

    class Meta:
        model = FilesUpload
        fields = ('id', 'file_name', 'user')
        read_only_fields = ('id', )

from django.urls import path, include

from rest_framework_nested.routers import DefaultRouter

from rest_framework_simplejwt import views as jwt_views
from rest_auth.views import UserDetailsView
from rest_auth.registration.views import RegisterView

from test_cd_core import views


app_name = 'test_cd_core'


file_upload_router = DefaultRouter(trailing_slash=False)
file_upload_router.register(r'file/upload', views.FileUpload, 'file/upload')

file_router = DefaultRouter(trailing_slash=False)
file_router.register(r'file', views.FileUploadViewSet, 'file')


urlpatterns = [
    path('user', UserDetailsView.as_view(), name='user'),
    path('user/create', RegisterView().as_view(), name='create_account'),
    path('user/auth', jwt_views.TokenObtainPairView.as_view(), name='token_obtain_pair'),
    path('', include(file_upload_router.urls)),
    path('', include(file_router.urls)),
]

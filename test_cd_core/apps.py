from django.apps import AppConfig


class TesteCoreConfig(AppConfig):
    name = 'test_cd_core'

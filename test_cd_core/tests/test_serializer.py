import uuid
import pytest

from test_cd_core.serializers import FilesUploadSerializer


pytestmark = [pytest.mark.django_db, pytest.mark.serial]


def test_should_serialize_files_upload(files_upload):
    # GIVEN
    # WHEN
    serializer = FilesUploadSerializer(files_upload)

    # THEN
    assert isinstance(serializer.data, dict)
    assert serializer.data.get('id') is not None
    assert serializer.data.get('file_name') == 'Arquivo_Teste.txt'
    assert serializer.data.get('user')


def test_should_deserialize_files_upload(user):
    # GIVEN
    id = uuid.uuid4()
    files_upload_data = {
        'id': id,
        'file_name': 'Arquivo_Teste.txt',
        'user': user.id
    }

    # WHEN
    serializer = FilesUploadSerializer(data=files_upload_data)

    # THEN
    assert serializer.is_valid()

    files_upload = serializer.save(id=id)
    assert files_upload.id
    assert files_upload.file_name == 'Arquivo_Teste.txt'
    assert files_upload.user.id == user.id

import pytest

from test_cd_core.models import FilesUpload


pytestmark = [pytest.mark.django_db, pytest.mark.serial]


def test_should_create_to_do_list(files_upload, user):
    assert FilesUpload.objects.count() == 1
    assert files_upload.id is not None
    assert files_upload.file_name == 'Arquivo_Teste.txt'
    assert files_upload.user.id == user.id

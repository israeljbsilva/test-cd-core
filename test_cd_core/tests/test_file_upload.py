import json
from http import HTTPStatus


def test_should_upload_file(customer_client, user, tmpdir):
    # GIVEN
    fd = tmpdir.mkdir('uploads').join('test.txt')
    fd.write('TEST1;TEST2;TEST3\nTEST4;TEST5;TEST6\nTEST7;TEST8;TEST9\n')

    # WHEN
    response = customer_client.post(
        '/api/v1/file/upload', data=fd.read(), content_type='multipart/form-data',
        **{'user': str(user.id)},
        **{'HTTP_USER': str(user.id), 'HTTP_CONTENT_DISPOSITION': f'attachment;filename={fd.basename}'}
    )

    # THEN
    assert response.status_code == HTTPStatus.OK


def test_not_should_upload_file_not_found(customer_client, user, tmpdir):
    # GIVEN
    fd = tmpdir.mkdir('uploads').join('fake.txt')

    # WHEN
    response = customer_client.post(
        '/api/v1/file/upload', data=b'', content_type='multipart/form-data',
        **{'user': str(user.id)},
        **{'HTTP_USER': str(user.id), 'HTTP_CONTENT_DISPOSITION': f'attachment;filename={fd.basename}'}
    )

    # THEN
    assert response.status_code == HTTPStatus.BAD_REQUEST
    assert response.json() == ['File not found.']


def test_should_list_upload_file(customer_client, user, tmpdir):
    # GIVEN
    fd = tmpdir.mkdir('uploads').join('test.txt')
    fd.write('TEST1;TEST2;TEST3\nTEST4;TEST5;TEST6\nTEST7;TEST8;TEST9\n')

    customer_client.post(
        '/api/v1/file/upload', data=fd.read(), content_type='multipart/form-data',
        **{'user': str(user.id)},
        **{'HTTP_USER': str(user.id), 'HTTP_CONTENT_DISPOSITION': f'attachment;filename={fd.basename}'}
    )

    # WHEN
    response = customer_client.get(f'/api/v1/file?user={str(user.id)}')

    # THEN
    assert response.status_code == HTTPStatus.OK
    content = json.loads(response.content)[0]
    assert content['id'] is not None
    assert content['file_name'] == 'test.txt'
    assert content['user'] == 1

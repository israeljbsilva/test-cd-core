import uuid

from django.db import models
from django.db.models.fields import CharField, UUIDField
from django.conf import settings

from django_extensions.db.models import TimeStampedModel


class FilesUpload(TimeStampedModel, models.Model):
    id = UUIDField(primary_key=True, default=uuid.uuid4, serialize=False, editable=False, unique=True)
    file_name = CharField('FILE_NAME', max_length=256, null=False)
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)

    class Meta:
        db_table = 'FileUpload'
        verbose_name = 'file_upload'
        verbose_name_plural = 'files_upload'

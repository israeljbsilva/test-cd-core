import uuid

import factory

from django.contrib.auth.hashers import make_password
from django.contrib.auth.models import User

from ..models import FilesUpload


class UserFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = User

    first_name = factory.Faker('first_name')
    last_name = factory.Faker('last_name')
    username = factory.Faker('email')
    password = factory.LazyFunction(lambda: make_password('12345678a'))
    is_staff = True
    is_superuser = True


class FilesUploadFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = FilesUpload

    id = uuid.uuid4()
    file_name = 'Arquivo_Teste.txt'
    user = factory.SubFactory(UserFactory)

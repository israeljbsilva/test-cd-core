PIP := pip install -r

PROJECT_NAME := test-cd-core
PYTHON_VERSION := 3.6.6
VENV_NAME := $(PROJECT_NAME)-$(PYTHON_VERSION)

# Environment setup
.pip:
	pip install pip --upgrade

setup: .pip
	$(PIP) requirements/base.txt

setup-dev: .pip
	$(PIP) requirements/local.txt

.create-venv:
	virtualenv venv
	source venv/bin/activate

create-venv: .create-venv setup

code-convention:
	flake8
	pycodestyle

# Tests
test:
	py.test --cov-report=term-missing  --cov-report=html --cov=.

all: create-venv setup-dev

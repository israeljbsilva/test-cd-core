from django.contrib import admin
from django.conf.urls import include
from django.urls import path

from drf_yasg import openapi
from drf_yasg.views import get_schema_view

from test_cd_core import views
from test_cd_core import urls


api_path = 'api/v1/'

schema_view = get_schema_view(
    openapi.Info(title="Test CD Core", default_version='v1'),
    public=True,
)

urlpatterns = [
    path('admin/', admin.site.urls),
    path('ping', views.ping),
    path('api-docs/', schema_view.with_ui('swagger', cache_timeout=None), name='schema-swagger-ui'),
    path(api_path, include(urls, urls.app_name))
]

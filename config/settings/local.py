"""
Local settings

- Run in Debug mode

- Use console backend for emails

- Add Django Debug Toolbar
- Add django-extensions as app
"""

from .base import *  # noqa

# DEBUG
# ------------------------------------------------------------------------------
DEBUG = env.bool('DJANGO_DEBUG', default=True)  # noqa
TEMPLATES[0]['OPTIONS']['debug'] = DEBUG  # noqa
TEMPLATES[0]['OPTIONS']['context_processors'].append('django.template.context_processors.debug')  # noqa

# SECRET CONFIGURATION
# ------------------------------------------------------------------------------
# See: https://docs.djangoproject.com/en/dev/ref/settings/#secret-key
# Note: This key only used for development and testing.
SECRET_KEY = env('DJANGO_SECRET_KEY', default='k)v#j3!ci6be%zdau6%1dxu1y&!c7yr0d=tu5bbf4ksjc5ge5l')  # noqa

# CACHING
# ------------------------------------------------------------------------------
CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.locmem.LocMemCache',
        'LOCATION': ''
    }
}

INTERNAL_IPS = ['127.0.0.1', '0.0.0.0']

FROM python:3.6-alpine

ARG arg_revision=''
ARG arg_build_date=''

ENV GUNICORN_WORKER_CLASS=sync \
    GUNICORN_WORKER_COUNT=4 \
    GUNICORN_PORT=8080 \
    WEB_CONCURRENCY=1 \
    REVISION=$arg_revision \
    BUILD_DATE=$arg_build_date \
    PATH=/app/.local/bin/:$PATH

EXPOSE $GUNICORN_PORT

COPY . /app
WORKDIR /app

RUN apk add --no-cache bash libpq \
    && apk add --update --no-cache --virtual deps make gcc libc-dev libmagic git postgresql-dev python3-dev musl-dev \
    && make setup \
    && chmod ugo+x run.sh \
    && chmod ugo+rwx -R /app \
    && apk del deps

CMD ./run.sh

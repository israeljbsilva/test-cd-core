# test-cd-core
Servidor http (django) contendo 2 serviços:
- Login (username, password)
- Upload (file)

### Build
<a href="https://gitlab.com/israeljbsilva/test-cd-core/commits/master"><img alt="build status" src="https://gitlab.com/israeljbsilva/test-cd-core/badges/master/build.svg" /></a>


# Link da aplicação no HEROKU
- https://test-cd-core.herokuapp.com/api/v1/


# Link da aplicação no GITLAB
- https://gitlab.com/israeljbsilva/test-cd-core

# Ferramentas usadas:
- Python 3.6
- Django
- SQLite / Postgres
- Git
- GitLab CI / CD
- Heroku

# Ferramentas de automação:
Este projeto usa o `Makefile`como ferramenta de automação.


# Set-up Virtual Environment
O seguinte comando instala a ferramenta `virtual-env`, usada para criar/gerenciar ambientes virtuais:

```bash
sudo pip3 install virtualenv 
```
Depois disso, acesse o diretório do projeto e execute `make all` para recriar o ambiente virtual e instalar as dependências.


# Executa a aplicação localmente:
```
python manage.py runserver
```

# Tests

## Roda os testes com o SQLite:
```
$ make test
```

# Conveções de código
## Executa convenções e métricas no código:
```
$ make code-convention
```

# Documentação da API
A documentação da API está disponível no endpoint `/api-docs/`.
- https://test-cd-core.herokuapp.com/api-docs/